package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.api.service.ITaskService;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.*;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.repository.TaskRepository;

import java.util.Collections;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.TaskTestData.*;
import static ru.t1.azarin.tm.constant.UserTestData.USER1;
import static ru.t1.azarin.tm.constant.UserTestData.USER_UNREGISTRY_ID;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @Nullable
    private final Integer nullInteger = null;

    @Before
    public void before() {
        repository.add(USER1_TASK1);
        repository.add(USER1_TASK2);
    }

    @After
    public void after() {
        repository.remove(USER1_TASK1);
        repository.remove(USER1_TASK2);
    }

    @Test
    public void add() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(emptyString, new Task()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(nullString, new Task()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(USER1.getId(), null));
        service.clear(USER1.getId());
        Assert.assertEquals(0, service.findAll(USER1.getId()).size());
        service.add(USER1.getId(), new Task());
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void create() {
        service.remove(USER1_TASK1);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(emptyString, USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(nullString, USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(USER1.getId(), emptyString, USER1_TASK1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(USER1.getId(), nullString, USER1_TASK1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(USER1.getId(), USER1_TASK1.getName(), emptyString));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(USER1.getId(), USER1_TASK1.getName(), nullString));
        @Nullable final Task task = service.create(USER1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription());
        Assert.assertEquals(USER1_TASK1.getName(), task.getName());
        Assert.assertEquals(USER1_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(USER1_TASK1.getUserId(), task.getUserId());
        Assert.assertEquals(service.findOneById(USER1.getId(), task.getId()), task);
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(nullString));
        service.clear(USER1.getId());
        Assert.assertEquals(0, service.findAll(USER1.getId()).size());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusById(emptyString, USER1_TASK1.getId(), Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusById(nullString, USER1_TASK1.getId(), Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeTaskStatusById(USER1.getId(), emptyString, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeTaskStatusById(USER1.getId(), nullString, Status.COMPLETED));
        @Nullable final Task task = service.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusByIndex(emptyString, service.findAll().indexOf(USER1_TASK1), Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusByIndex(nullString, service.findAll().indexOf(USER1_TASK1), Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeTaskStatusByIndex(USER1.getId(), nullInteger, Status.COMPLETED));
        @Nullable final Task task = service.changeTaskStatusByIndex(USER1.getId(), service.findAll().indexOf(USER1_TASK1), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void existById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1.getId(), emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1.getId(), nullString));
        Assert.assertTrue(service.existById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(service.existById(USER_UNREGISTRY_ID, USER1_TASK1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(nullString));
        Assert.assertEquals(USER1_TASK_LIST, service.findAll(USER1.getId()));
        Assert.assertEquals(0, service.findAll(USER_UNREGISTRY_ID).size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), nullString));
        @Nullable final Task task = service.findOneById(USER1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_TASK1, task);
    }

    @Test
    public void findOneByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(emptyString, service.findAll().indexOf(USER1_TASK1)));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(nullString, service.findAll().indexOf(USER1_TASK1)));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(USER1.getId(), nullInteger));
        @Nullable final Integer index = service.findAll().indexOf(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, service.findOneByIndex(index));
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAllByProjectId(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAllByProjectId(nullString, USER1_PROJECT1.getId()));
        Assert.assertEquals(service.findAllByProjectId(USER1.getId(), nullString), Collections.emptyList());
    }


    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(emptyString, USER1_TASK1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(nullString, USER1_TASK1));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(USER1.getId(), null));
        service.remove(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), nullString));
        service.removeById(USER1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void removeByIndex() {
        @Nullable final Integer index = repository.findAll().indexOf(USER1_TASK1);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(emptyString, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(nullString, index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(USER1.getId(), nullInteger));
        service.removeByIndex(USER1.getId(), index);
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(emptyString, USER1_TASK1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(nullString, USER1_TASK1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(USER1.getId(), emptyString, USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(USER1.getId(), nullString, USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(USER1.getId(), USER1_TASK1.getId(), emptyString, USER1_TASK1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(USER1.getId(), USER1_TASK1.getId(), nullString, USER1_TASK1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(USER1.getId(), USER1_TASK1.getId(), USER1_TASK1.getName(), emptyString));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(USER1.getId(), USER1_TASK1.getId(), USER1_TASK1.getName(), nullString));
        service.updateById(USER1.getId(), USER1_TASK1.getId(), testString, USER1_TASK1.getDescription());
        Assert.assertEquals(testString, USER1_TASK1.getName());
    }

    @Test
    public void updateByIndex() {
        @Nullable final Integer index = repository.findAll().indexOf(USER1_TASK1);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(emptyString, index, USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(nullString, index, USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(USER1.getId(), nullInteger, USER1_TASK1.getName(), USER1_TASK1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, emptyString, USER1_TASK1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, nullString, USER1_TASK1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, USER1_TASK1.getName(), emptyString));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, USER1_TASK1.getName(), nullString));
        service.updateByIndex(USER1.getId(), index, testString, USER1_TASK1.getDescription());
        Assert.assertEquals(testString, USER1_TASK1.getName());
    }

}
