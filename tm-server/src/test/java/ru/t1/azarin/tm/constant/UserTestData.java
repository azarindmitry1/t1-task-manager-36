package ru.t1.azarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.service.PropertyService;
import ru.t1.azarin.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static String USER1_LOGIN = "test_user";

    @NotNull
    public final static String USER1_PASSWORD = "test_user";

    @NotNull
    public final static String USER1_EMAIL = "test_user@email.com";

    @NotNull
    public final static String ADMIN_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_PASSWORD = "admin";

    @NotNull
    public final static String ADMIN_EMAIL = "admin@email.com";

    @NotNull
    public final static String USER_UNREGISTRY_ID = "test_id";

    @NotNull
    public final static String USER_UNREGISTRY_LOGIN = "test_login";

    @NotNull
    public final static String USER_UNREGISTRY_EMAIL = "test_email";

    @NotNull
    public final static String USER_UNREGISTRY_PASSWORD = "test_password";

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User ADMIN = new User();

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER1, ADMIN);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    static {
        USER1.setLogin(USER1_LOGIN);
        USER1.setPasswordHash(HashUtil.salt(propertyService, USER1_PASSWORD));
        USER1.setEmail(USER1_EMAIL);

        ADMIN.setLogin(ADMIN_LOGIN);
        ADMIN.setPasswordHash(HashUtil.salt(propertyService, ADMIN_PASSWORD));
        ADMIN.setEmail(ADMIN_EMAIL);
    }

}
