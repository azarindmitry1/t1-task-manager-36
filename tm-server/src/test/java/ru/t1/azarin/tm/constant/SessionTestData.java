package ru.t1.azarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.model.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static Session USER1_SESSION1 = new Session();

    @NotNull
    public final static Session ADMIN_SESSION1 = new Session();

    @NotNull
    public final static List<Session> USER1_SESSION_LIST = Arrays.asList(USER1_SESSION1);

    @NotNull
    public final static List<Session> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1);

    @NotNull
    public final static List<Session> SESSION_LIST = new ArrayList<>();

    static {
        USER1_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USER1.getId()));
        ADMIN_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.ADMIN.getId()));

        SESSION_LIST.addAll(USER1_SESSION_LIST);
        SESSION_LIST.addAll(ADMIN_SESSION_LIST);
    }

}
