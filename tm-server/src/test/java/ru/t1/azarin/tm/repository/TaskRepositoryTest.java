package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.model.Task;

import static ru.t1.azarin.tm.constant.TaskTestData.*;
import static ru.t1.azarin.tm.constant.UserTestData.USER1;

public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @Before
    public void before() {
        repository.add(USER1_TASK1);
        repository.add(USER1_TASK2);
    }

    @After
    public void after() {
        repository.remove(USER1_TASK1);
        repository.remove(USER1_TASK2);
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(USER1.getId(), USER1_TASK1));
        @Nullable final Task task = repository.findOneById(USER1.getId(), USER1_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER1_TASK1, task);
    }

    @Test
    public void create() {
        @Nullable final Task task = repository.create(USER1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription());
        Assert.assertEquals(USER1_TASK1.getName(), task.getName());
        Assert.assertEquals(USER1_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(USER1_TASK1.getUserId(), task.getUserId());
        Assert.assertEquals(repository.findOneById(USER1.getId(), task.getId()), task);
    }

    @Test
    public void clear() {
        Assert.assertFalse(repository.findAll().isEmpty());
        repository.clear(USER1.getId());
        Assert.assertEquals(0, repository.findAll(USER1.getId()).size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER1_TASK_LIST, repository.findAll());
    }

    @Test
    public void findOneById() {
        @Nullable final Task task = repository.findOneById(USER1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_TASK1, task);
    }

    @Test
    public void findOneByIndex() {
        @Nullable final Task task = repository.findOneByIndex(USER1.getId(), repository.findAll().indexOf(USER1_TASK1));
        Assert.assertEquals(USER1_TASK1, task);
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.existById(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void remove() {
        @Nullable final Task task = repository.findOneById(USER1.getId(), USER1_TASK1.getId());
        repository.remove(USER1.getId(), task);
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void removeById() {
        @Nullable final Task task = repository.findOneById(USER1.getId(), USER1_TASK1.getId());
        repository.removeById(USER1.getId(), task.getId());
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void removeByIndex() {
        final Integer index = repository.findAll().indexOf(USER1_TASK1);
        repository.removeByIndex(USER1.getId(), index);
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_TASK1.getId()));
    }

}
