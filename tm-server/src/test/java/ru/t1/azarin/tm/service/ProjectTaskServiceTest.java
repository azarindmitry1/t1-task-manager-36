package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.api.service.IProjectTaskService;
import ru.t1.azarin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.azarin.tm.exception.field.TaskIdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.repository.ProjectRepository;
import ru.t1.azarin.tm.repository.TaskRepository;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT2;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK2;
import static ru.t1.azarin.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @Nullable
    private final Integer nullInteger = null;

    @Before
    public void before() {
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER1_PROJECT2);
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER1_TASK2);
    }

    @After
    public void after() {
        projectRepository.remove(USER1_PROJECT1);
        projectRepository.remove(USER1_PROJECT2);
        taskRepository.remove(USER1_TASK1);
        taskRepository.remove(USER1_TASK2);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), nullString, USER1_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), emptyString));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), nullString));
        @Nullable final Task task = taskRepository.create(USER1.getId(), testString, testString);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), task.getId());
        Assert.assertEquals(task.getProjectId(), USER1_PROJECT1.getId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskToProject(emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskToProject(nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), nullString, USER1_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), emptyString));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), nullString));
        service.unbindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(USER1.getId(), emptyString));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(USER1.getId(), nullString));
        service.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
    }

}
