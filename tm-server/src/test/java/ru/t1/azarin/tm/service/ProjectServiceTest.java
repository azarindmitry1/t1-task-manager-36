package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.service.IProjectService;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.*;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.repository.ProjectRepository;

import static ru.t1.azarin.tm.constant.ProjectTestData.*;
import static ru.t1.azarin.tm.constant.UserTestData.USER1;
import static ru.t1.azarin.tm.constant.UserTestData.USER_UNREGISTRY_ID;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "TEST_STRING";

    @Nullable
    private final Integer nullInteger = null;

    @Before
    public void before() {
        repository.add(USER1_PROJECT1);
        repository.add(USER1_PROJECT2);
    }

    @After
    public void after() {
        repository.remove(USER1_PROJECT1);
        repository.remove(USER1_PROJECT2);
    }

    @Test
    public void add() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(emptyString, new Project()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(nullString, new Project()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(USER1.getId(), null));
        service.clear(USER1.getId());
        Assert.assertEquals(0, service.findAll(USER1.getId()).size());
        service.add(USER1.getId(), new Project());
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void create() {
        service.remove(USER1_PROJECT1);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(emptyString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(nullString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(USER1.getId(), emptyString, USER1_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(USER1.getId(), nullString, USER1_PROJECT1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(USER1.getId(), USER1_PROJECT1.getName(), emptyString));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(USER1.getId(), USER1_PROJECT1.getName(), nullString));
        @Nullable final Project project = service.create(USER1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription());
        Assert.assertEquals(USER1_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER1_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER1_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(service.findOneById(USER1.getId(), project.getId()), project);
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(nullString));
        service.clear(USER1.getId());
        Assert.assertEquals(0, service.findAll(USER1.getId()).size());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById(emptyString, USER1_PROJECT1.getId(), Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById(nullString, USER1_PROJECT1.getId(), Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeProjectStatusById(USER1.getId(), emptyString, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeProjectStatusById(USER1.getId(), nullString, Status.COMPLETED));
        @Nullable final Project project = service.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusByIndex(emptyString, service.findAll().indexOf(USER1_PROJECT1), Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusByIndex(nullString, service.findAll().indexOf(USER1_PROJECT1), Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeProjectStatusByIndex(USER1.getId(), nullInteger, Status.COMPLETED));
        @Nullable final Project project = service.changeProjectStatusByIndex(USER1.getId(), service.findAll().indexOf(USER1_PROJECT1), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void existById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1.getId(), emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1.getId(), nullString));
        Assert.assertTrue(service.existById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(service.existById(USER_UNREGISTRY_ID, USER1_PROJECT1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(nullString));
        Assert.assertEquals(USER1_PROJECT_LIST, service.findAll(USER1.getId()));
        Assert.assertEquals(0, service.findAll(USER_UNREGISTRY_ID).size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), nullString));
        @Nullable final Project project = service.findOneById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(emptyString, service.findAll().indexOf(USER1_PROJECT1)));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(nullString, service.findAll().indexOf(USER1_PROJECT1)));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(USER1.getId(), nullInteger));
        @Nullable final Integer index = service.findAll().indexOf(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, service.findOneByIndex(index));
    }

    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(emptyString, USER1_PROJECT1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(nullString, USER1_PROJECT1));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(USER1.getId(), null));
        service.remove(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(USER1.getId(), nullString));
        service.removeById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void removeByIndex() {
        @Nullable final Integer index = repository.findAll().indexOf(USER1_PROJECT1);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(emptyString, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(nullString, index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(USER1.getId(), nullInteger));
        service.removeByIndex(USER1.getId(), index);
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(emptyString, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(nullString, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(USER1.getId(), emptyString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(USER1.getId(), nullString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(USER1.getId(), USER1_PROJECT1.getId(), emptyString, USER1_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(USER1.getId(), USER1_PROJECT1.getId(), nullString, USER1_PROJECT1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(USER1.getId(), USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), emptyString));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(USER1.getId(), USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), nullString));
        service.updateById(USER1.getId(), USER1_PROJECT1.getId(), testString, USER1_PROJECT1.getDescription());
        Assert.assertEquals(testString, USER1_PROJECT1.getName());
    }

    @Test
    public void updateByIndex() {
        @Nullable final Integer index = repository.findAll().indexOf(USER1_PROJECT1);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(emptyString, index, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(nullString, index, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(USER1.getId(), nullInteger, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, emptyString, USER1_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, nullString, USER1_PROJECT1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, USER1_PROJECT1.getName(), emptyString));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateByIndex(USER1.getId(), index, USER1_PROJECT1.getName(), nullString));
        service.updateByIndex(USER1.getId(), index, testString, USER1_PROJECT1.getDescription());
        Assert.assertEquals(testString, USER1_PROJECT1.getName());
    }

}
