package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Project;

import static ru.t1.azarin.tm.constant.ProjectTestData.*;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @Before
    public void before() {
        repository.add(USER1_PROJECT1);
        repository.add(USER1_PROJECT2);
    }

    @After
    public void after() {
        repository.remove(USER1_PROJECT1);
        repository.remove(USER1_PROJECT2);
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(USER1.getId(), USER1_PROJECT1));
        @Nullable final Project project = repository.findOneById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER1_PROJECT1, project);
    }

    @Test
    public void create() {
        @Nullable final Project project = repository.create(USER1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription());
        Assert.assertEquals(USER1_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER1_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER1_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(repository.findOneById(USER1.getId(), project.getId()), project);
    }

    @Test
    public void clear() {
        Assert.assertFalse(repository.findAll().isEmpty());
        repository.clear(USER1.getId());
        Assert.assertEquals(0, repository.findAll(USER1.getId()).size());
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.existById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(repository.existById(USER_UNREGISTRY_ID, USER1_PROJECT1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER1_PROJECT_LIST, repository.findAll());
    }

    @Test
    public void findOneById() {
        @Nullable final Project project = repository.findOneById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() {
        @Nullable final Project project = repository.findOneByIndex(USER1.getId(), repository.findAll().indexOf(USER1_PROJECT1));
        Assert.assertEquals(USER1_PROJECT1, project);
    }

    @Test
    public void remove() {
        @Nullable final Project project = repository.findOneById(USER1.getId(), USER1_PROJECT1.getId());
        repository.remove(USER1.getId(), project);
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void removeById() {
        @Nullable final Project project = repository.findOneById(USER1.getId(), USER1_PROJECT1.getId());
        repository.removeById(USER1.getId(), project.getId());
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void removeByIndex() {
        @Nullable final Integer index = repository.findAll().indexOf(USER1_PROJECT1);
        repository.removeByIndex(USER1.getId(), index);
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
    }

}
