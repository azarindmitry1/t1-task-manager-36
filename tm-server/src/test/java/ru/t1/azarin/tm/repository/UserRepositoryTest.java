package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.service.PropertyService;
import ru.t1.azarin.tm.util.HashUtil;

import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Before
    public void before() {
        repository.add(USER1);
        repository.add(ADMIN);
    }

    @After
    public void after() {
        repository.remove(USER1);
        repository.remove(ADMIN);
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(USER1));
        @Nullable final User user = repository.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1, user);
    }

    @Test
    public void clear() {
        Assert.assertFalse(repository.findAll().isEmpty());
        repository.clear();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void create() {
        @Nullable final User user = repository.create(USER1_LOGIN, HashUtil.salt(propertyService, USER1_PASSWORD), USER1_EMAIL);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getLogin(), user.getLogin());
        Assert.assertEquals(USER1.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(USER1.getEmail(), user.getEmail());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER_LIST, repository.findAll());
    }

    @Test
    public void findOneById() {
        @Nullable final User user = repository.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1, user);
    }

    @Test
    public void findOneByIndex() {
        @Nullable final User user = repository.findOneByIndex(repository.findAll().indexOf(USER1));
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1, user);
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = repository.findByLogin(USER1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1, user);
    }

    @Test
    public void findByEmail() {
        @Nullable final User user = repository.findByEmail(USER1.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1, user);
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.existById(USER1.getId()));
        Assert.assertFalse(repository.existById(USER_UNREGISTRY_ID));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(repository.isLoginExist(USER_UNREGISTRY_LOGIN));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(repository.isEmailExist(USER_UNREGISTRY_EMAIL));
    }

    @Test
    public void remove() {
        @Nullable final User user = repository.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        repository.remove(USER1);
        Assert.assertNull(repository.findOneById(USER1.getId()));
    }

    @Test
    public void removeById() {
        @Nullable final User user = repository.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        repository.removeById(USER1.getId());
        Assert.assertNull(repository.findOneById(USER1.getId()));
    }

    @Test
    public void removeByIndex() {
        final Integer index = repository.findAll().indexOf(USER1);
        Assert.assertNotNull(index);
        repository.removeByIndex(index);
        Assert.assertNull(repository.findOneById(USER1.getId()));
    }

}
