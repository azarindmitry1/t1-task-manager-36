package ru.t1.azarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static Task USER1_TASK1 = new Task();

    @NotNull
    public final static Task USER1_TASK2 = new Task();

    @NotNull
    public final static Task ADMIN_TASK1 = new Task();

    @NotNull
    public final static Task ADMIN_TASK2 = new Task();

    @NotNull
    public final static List<Task> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2);

    @NotNull
    public final static List<Task> ADMIN_TASK_LIST = Arrays.asList(ADMIN_TASK1, ADMIN_TASK2);

    @NotNull
    public final static List<Task> TASK_LIST = new ArrayList<>();

    static {
        USER1_TASK_LIST.forEach(project -> project.setUserId(UserTestData.USER1.getId()));
        ADMIN_TASK_LIST.forEach(project -> project.setUserId(UserTestData.ADMIN.getId()));

        TASK_LIST.addAll(USER1_TASK_LIST);
        TASK_LIST.addAll(ADMIN_TASK_LIST);

        for (int i = 0; i < TASK_LIST.size(); i++) {
            @NotNull final Task task = TASK_LIST.get(i);
            task.setName("task-" + i);
            task.setDescription("description-" + i);
            task.setProjectId(ProjectTestData.USER1_PROJECT1.getId());
        }
    }

}
