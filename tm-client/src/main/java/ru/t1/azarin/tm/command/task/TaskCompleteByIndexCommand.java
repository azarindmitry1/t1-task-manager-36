package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "task-complete-by-index";

    @NotNull
    public final static String DESCRIPTION = "Complete task by index.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken());
        request.setIndex(index);
        getTaskEndpoint().completeTaskByIndexResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
