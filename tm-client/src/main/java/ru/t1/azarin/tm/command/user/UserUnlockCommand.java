package ru.t1.azarin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.user.UserUnlockRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-unlock";

    @NotNull
    public final static String DESCRIPTION = "Unlock user.";

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().unlockResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
