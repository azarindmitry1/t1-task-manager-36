package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.user.*;
import ru.t1.azarin.tm.dto.response.user.UserRegistryResponse;
import ru.t1.azarin.tm.dto.response.user.UserUpdateProfileResponse;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @Nullable
    private String adminToken;

    @Before
    public void before() {
        adminToken = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
    }

    @After
    public void after() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(adminToken);
        authEndpoint.logout(request);
    }

    @Test
    public void changePassword() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePasswordResponse(new UserChangePasswordRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePasswordResponse(new UserChangePasswordRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePasswordResponse(new UserChangePasswordRequest(nullString))
        );
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(adminToken);
        changePasswordRequest.setNewPassword(testString);
        userEndpoint.changePasswordResponse(changePasswordRequest);
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(adminToken);
        authEndpoint.logout(request);
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest("admin", testString)).getToken());
        @NotNull final UserChangePasswordRequest changePasswordRequest1 = new UserChangePasswordRequest(adminToken);
        changePasswordRequest1.setNewPassword("admin");
        userEndpoint.changePasswordResponse(changePasswordRequest1);
    }

    @Test
    public void lockUser() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockResponse(new UserLockRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockResponse(new UserLockRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockResponse(new UserLockRequest(nullString))
        );
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin("test_user");
        userEndpoint.lockResponse(lockRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(logoutRequest);
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(authEndpoint.login(new UserLoginRequest("test_user", "test_user")).getToken()))
        );
    }

    @Test
    public void unlockUser() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockResponse(new UserUnlockRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockResponse(new UserUnlockRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockResponse(new UserUnlockRequest(nullString))
        );
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin("test_user");
        userEndpoint.unlockResponse(unlockRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(logoutRequest);
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest("test_user", "test_user")));
    }

    @Test
    public void registry() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registryResponse(new UserRegistryRequest(emptyString, testString, testString))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registryResponse(new UserRegistryRequest(testString, emptyString, testString))
        );
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(testString);
        request.setPassword(testString);
        request.setEmail(testString);
        @Nullable final UserRegistryResponse response = userEndpoint.registryResponse(request);
        Assert.assertNotNull(response.getUser());
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(testString);
        userEndpoint.removeResponse(removeRequest);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(testString, testString)));
    }

    @Test
    public void remove() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeResponse(new UserRemoveRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeResponse(new UserRemoveRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeResponse(new UserRemoveRequest(nullString))
        );
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(testString);
        registryRequest.setPassword(testString);
        registryRequest.setEmail(testString);
        userEndpoint.registryResponse(registryRequest);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(testString);
        userEndpoint.removeResponse(removeRequest);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(testString, testString)));
    }

    @Test
    public void updateProfile() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfileResponse(new UserUpdateProfileRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfileResponse(new UserUpdateProfileRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfileResponse(new UserUpdateProfileRequest(nullString))
        );
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(adminToken);
        request.setFirstName(testString);
        request.setMiddleName(testString);
        request.setLastName(testString);
        @Nullable final UserUpdateProfileResponse response = userEndpoint.updateProfileResponse(request);
        Assert.assertEquals(testString, response.getUser().getFirstName());
        Assert.assertEquals(testString, response.getUser().getMiddleName());
        Assert.assertEquals(testString, response.getUser().getLastName());
    }

}
