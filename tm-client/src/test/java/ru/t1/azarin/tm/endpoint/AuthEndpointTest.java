package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.azarin.tm.dto.response.user.UserLoginResponse;
import ru.t1.azarin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.azarin.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final String adminLogin = "admin";

    @NotNull
    private final String adminPassword = "admin";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(emptyString, emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(nullString, nullString))
        );
        @NotNull final UserLoginRequest request = new UserLoginRequest(adminLogin, adminPassword);
        @Nullable final UserLoginResponse response = authEndpoint.login(request);
        Assert.assertNotNull(response.getToken());
        Assert.assertTrue(response.getSuccess());
    }

    @Test
    public void logout() {
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.logout(new UserLogoutRequest())
        );
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(adminLogin, adminPassword);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(loginResponse.getToken());
        @Nullable final UserLogoutResponse logoutResponse = authEndpoint.logout(logoutRequest);
        Assert.assertNotNull(logoutResponse);
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.logout(new UserLogoutRequest(testString))
        );
    }

    @Test
    public void viewProfile() {
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.viewProfile(new UserViewProfileRequest())
        );
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(adminLogin, adminPassword);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        @NotNull final UserViewProfileRequest viewProfileRequest = new UserViewProfileRequest(loginResponse.getToken());
        @Nullable final UserViewProfileResponse viewProfileResponse = authEndpoint.viewProfile(viewProfileRequest);
        Assert.assertEquals(adminLogin, viewProfileResponse.getUser().getLogin());
    }

}
