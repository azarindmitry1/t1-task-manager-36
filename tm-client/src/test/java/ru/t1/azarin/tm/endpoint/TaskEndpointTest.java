package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.azarin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.project.ProjectClearRequest;
import ru.t1.azarin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.azarin.tm.dto.request.task.*;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.azarin.tm.dto.response.task.*;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @NotNull
    private final String testString2 = "testString2";

    @NotNull
    private final String testString3 = "testString3";

    @Nullable
    private String adminToken;

    @Nullable
    private Project adminProject1;

    @Nullable
    private Task adminTask1;

    @Nullable
    private Task adminTask2;

    @Before
    public void before() {
        adminToken = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();

        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(adminToken);
        createRequest.setName(testString);
        createRequest.setDescription(testString);
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createResponse(createRequest);
        adminProject1 = createResponse.getProject();

        @NotNull final TaskCreateRequest createRequest1 = new TaskCreateRequest(adminToken);
        createRequest1.setName(testString);
        createRequest1.setDescription(testString);
        @NotNull final TaskCreateResponse createResponse1 = taskEndpoint.createTaskResponse(createRequest1);
        adminTask1 = createResponse1.getTask();

        @NotNull final TaskCreateRequest createRequest2 = new TaskCreateRequest(adminToken);
        createRequest2.setName(testString2);
        createRequest2.setDescription(testString2);
        @NotNull final TaskCreateResponse createResponse2 = taskEndpoint.createTaskResponse(createRequest2);
        adminTask2 = createResponse2.getTask();
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(adminToken);
        projectEndpoint.clearResponse(clearRequest);

        @NotNull final TaskClearRequest clearRequest1 = new TaskClearRequest(adminToken);
        taskEndpoint.clearTaskResponse(clearRequest1);

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(adminToken);
        authEndpoint.logout(request);
    }

    @Test
    public void changeTaskStatusByIdResponse() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest(nullString))
        );
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(adminToken);
        request.setId(adminTask1.getId());
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void changeTaskStatusByIndexResponse() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndexResponse(new TaskChangeStatusByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndexResponse(new TaskChangeStatusByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndexResponse(new TaskChangeStatusByIndexRequest(nullString))
        );
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(adminToken);
        request.setIndex(0);
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void clearTask() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest(nullString))
        );
        @NotNull final TaskClearRequest request = new TaskClearRequest(adminToken);
        @Nullable final TaskClearResponse response = taskEndpoint.clearTaskResponse(request);
        Assert.assertNull(taskEndpoint.listTaskResponse(new TaskListRequest(adminToken)).getTasks());
    }

    @Test
    public void completeTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest(nullString))
        );
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(adminToken);
        request.setId(adminTask1.getId());
        @Nullable final TaskCompleteByIdResponse response = taskEndpoint.completeTaskByIdResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void completeTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIndexResponse(new TaskCompleteByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIndexResponse(new TaskCompleteByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIndexResponse(new TaskCompleteByIndexRequest(nullString))
        );
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(adminToken);
        request.setIndex(0);
        @Nullable final TaskCompleteByIndexResponse response = taskEndpoint.completeTaskByIndexResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest(nullString))
        );
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(adminToken);
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final TaskCreateResponse response = taskEndpoint.createTaskResponse(request);
        Assert.assertEquals(testString3, response.getTask().getName());
        Assert.assertEquals(testString3, response.getTask().getDescription());
    }

    @Test
    public void listTaskByProjectId() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest(nullString))
        );
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(adminToken);
        bindRequest.setProjectId(adminProject1.getId());
        bindRequest.setTaskId(adminTask1.getId());
        taskEndpoint.bindTaskToProjectResponse(bindRequest);
        @NotNull final TaskListByProjectIdRequest listRequest = new TaskListByProjectIdRequest(adminToken);
        listRequest.setId(adminProject1.getId());
        @Nullable final TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectIdResponse(listRequest);
        Assert.assertEquals(1, response.getTasks().size());
    }

    @Test
    public void list() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest(nullString))
        );
        @NotNull final TaskListRequest request = new TaskListRequest(adminToken);
        @Nullable final TaskListResponse response = taskEndpoint.listTaskResponse(request);
        Assert.assertEquals(2, response.getTasks().size());
    }

    @Test
    public void removeTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest(nullString))
        );
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(adminToken);
        request.setId(adminTask1.getId());
        @Nullable final TaskRemoveByIdResponse response = taskEndpoint.removeTaskByIdResponse(request);
        Assert.assertEquals(adminTask1.getId(), response.getTask().getId());
    }

    @Test
    public void removeTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIndexResponse(new TaskRemoveByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIndexResponse(new TaskRemoveByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIndexResponse(new TaskRemoveByIndexRequest(nullString))
        );
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(adminToken);
        request.setIndex(0);
        @Nullable final TaskRemoveByIndexResponse response = taskEndpoint.removeTaskByIndexResponse(request);
        Assert.assertEquals(adminTask1.getId(), response.getTask().getId());
    }

    @Test
    public void showTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest(nullString))
        );
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(adminToken);
        request.setId(adminTask1.getId());
        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskByIdResponse(request);
        Assert.assertEquals(adminTask1.getName(), response.getTask().getName());
        Assert.assertEquals(adminTask1.getDescription(), response.getTask().getDescription());
    }

    @Test
    public void showTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIndexResponse(new TaskShowByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIndexResponse(new TaskShowByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIndexResponse(new TaskShowByIndexRequest(nullString))
        );
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(adminToken);
        request.setIndex(0);
        @Nullable final TaskShowByIndexResponse response = taskEndpoint.showTaskByIndexResponse(request);
        Assert.assertEquals(adminTask1.getName(), response.getTask().getName());
        Assert.assertEquals(adminTask1.getDescription(), response.getTask().getDescription());
    }

    @Test
    public void startTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest(nullString))
        );
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(adminToken);
        request.setId(adminTask1.getId());
        @Nullable final TaskStartByIdResponse response = taskEndpoint.startTaskByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void startByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIndexResponse(new TaskStartByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIndexResponse(new TaskStartByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIndexResponse(new TaskStartByIndexRequest(nullString))
        );
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(adminToken);
        request.setIndex(0);
        @Nullable final TaskStartByIndexResponse response = taskEndpoint.startTaskByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest(nullString))
        );
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(adminToken);
        request.setProjectId(adminProject1.getId());
        request.setTaskId(adminTask1.getId());
        @Nullable final TaskBindToProjectResponse response = taskEndpoint.bindTaskToProjectResponse(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest(nullString))
        );
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(adminToken);
        request.setProjectId(adminProject1.getId());
        request.setTaskId(adminTask1.getId());
        @Nullable final TaskUnbindFromProjectResponse response = taskEndpoint.unbindTaskFromProjectResponse(request);
        Assert.assertEquals(null, adminTask1.getProjectId());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest(nullString))
        );
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(adminToken);
        request.setId(adminTask1.getId());
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final TaskUpdateByIdResponse response = taskEndpoint.updateTaskByIdResponse(request);
        Assert.assertEquals(testString3, response.getTask().getName());
        Assert.assertEquals(testString3, response.getTask().getDescription());
    }

    @Test
    public void updateTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndexResponse(new TaskUpdateByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndexResponse(new TaskUpdateByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndexResponse(new TaskUpdateByIndexRequest(nullString))
        );
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(adminToken);
        request.setIndex(0);
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndexResponse(request);
        Assert.assertEquals(testString3, response.getTask().getName());
        Assert.assertEquals(testString3, response.getTask().getDescription());
    }

}
