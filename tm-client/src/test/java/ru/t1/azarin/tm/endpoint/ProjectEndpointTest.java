package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.project.*;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.response.project.*;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @NotNull
    private final String testString2 = "testString2";

    @NotNull
    private final String testString3 = "testString3";

    @Nullable
    private String adminToken;

    @Nullable
    private Project adminProject1;

    @Nullable
    private Project adminProject2;

    @Before
    public void before() {
        adminToken = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();

        @NotNull final ProjectCreateRequest createRequest1 = new ProjectCreateRequest(adminToken);
        createRequest1.setName(testString);
        createRequest1.setDescription(testString);
        @NotNull final ProjectCreateResponse createResponse1 = projectEndpoint.createResponse(createRequest1);
        adminProject1 = createResponse1.getProject();

        @NotNull final ProjectCreateRequest createRequest2 = new ProjectCreateRequest(adminToken);
        createRequest2.setName(testString2);
        createRequest2.setDescription(testString2);
        @NotNull final ProjectCreateResponse createResponse2 = projectEndpoint.createResponse(createRequest2);
        adminProject2 = createResponse2.getProject();
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(adminToken);
        projectEndpoint.clearResponse(clearRequest);

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(adminToken);
        authEndpoint.logout(request);
    }

    @Test
    public void changeStatusById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest(nullString))
        );
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(adminToken);
        request.setId(adminProject1.getId());
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final ProjectChangeStatusByIdResponse response = projectEndpoint.changeStatusByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIndexResponse(new ProjectChangeStatusByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIndexResponse(new ProjectChangeStatusByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIndexResponse(new ProjectChangeStatusByIndexRequest(nullString))
        );
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(adminToken);
        request.setIndex(0);
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeStatusByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void clear() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest(nullString))
        );
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(adminToken);
        @Nullable final ProjectClearResponse response = projectEndpoint.clearResponse(request);
        Assert.assertNull(projectEndpoint.listResponse(new ProjectListRequest(adminToken)).getProjects());
    }

    @Test
    public void completeById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest(nullString))
        );

        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(adminToken);
        request.setId(adminProject1.getId());
        @Nullable final ProjectCompleteByIdResponse response = projectEndpoint.completeByIdResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test
    public void completeByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIndexResponse(new ProjectCompleteByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIndexResponse(new ProjectCompleteByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIndexResponse(new ProjectCompleteByIndexRequest(nullString))
        );
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(adminToken);
        request.setIndex(0);
        @Nullable final ProjectCompleteByIndexResponse response = projectEndpoint.completeByIndexResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test
    public void create() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest(nullString))
        );
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(adminToken);
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final ProjectCreateResponse response = projectEndpoint.createResponse(request);
        Assert.assertEquals(testString3, response.getProject().getName());
        Assert.assertEquals(testString3, response.getProject().getDescription());
    }

    @Test
    public void list() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest(nullString))
        );
        @Nullable final ProjectListResponse response = projectEndpoint.listResponse(new ProjectListRequest(adminToken));
        Assert.assertNotNull(response.getProjects());
        Assert.assertEquals(2, response.getProjects().size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest(nullString))
        );
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(adminToken);
        request.setId(adminProject1.getId());
        @Nullable final ProjectRemoveByIdResponse response = projectEndpoint.removeByIdResponse(request);
        Assert.assertEquals(adminProject1.getId(), response.getProject().getId());
    }

    @Test
    public void removeByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIndexResponse(new ProjectRemoveByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIndexResponse(new ProjectRemoveByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIndexResponse(new ProjectRemoveByIndexRequest(nullString))
        );
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(adminToken);
        request.setIndex(0);
        @Nullable final ProjectRemoveByIndexResponse response = projectEndpoint.removeByIndexResponse(request);
        Assert.assertEquals(1, projectEndpoint.listResponse(new ProjectListRequest(adminToken)).getProjects().size());
    }

    @Test
    public void startById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest(nullString))
        );
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(adminToken);
        request.setId(adminProject1.getId());
        @Nullable final ProjectStartByIdResponse response = projectEndpoint.startByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void startByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIndexResponse(new ProjectStartByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIndexResponse(new ProjectStartByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIndexResponse(new ProjectStartByIndexRequest(nullString))
        );
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(adminToken);
        request.setIndex(0);
        @Nullable final ProjectStartByIndexResponse response = projectEndpoint.startByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest(nullString))
        );
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(adminToken);
        request.setId(adminProject1.getId());
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final ProjectUpdateByIdResponse response = projectEndpoint.updateByIdResponse(request);
        Assert.assertEquals(testString3, response.getProject().getName());
        Assert.assertEquals(testString3, response.getProject().getDescription());
    }

    @Test
    public void updateByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIndexResponse(new ProjectUpdateByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIndexResponse(new ProjectUpdateByIndexRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIndexResponse(new ProjectUpdateByIndexRequest(nullString))
        );
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(adminToken);
        request.setIndex(0);
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final ProjectUpdateByIndexResponse response = projectEndpoint.updateByIndexResponse(request);
        Assert.assertEquals(testString3, response.getProject().getName());
        Assert.assertEquals(testString3, response.getProject().getDescription());
    }

}
